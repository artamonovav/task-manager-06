package ru.t1.artamonov.tm;

import java.util.Scanner;

import static ru.t1.artamonov.tm.constant.TerminalConst.CMD_VERSION;

import static ru.t1.artamonov.tm.constant.TerminalConst.CMD_HELP;

import static ru.t1.artamonov.tm.constant.TerminalConst.CMD_ABOUT;

import static ru.t1.artamonov.tm.constant.TerminalConst.CMD_EXIT;

import static ru.t1.artamonov.tm.constant.ArgumentConst.ARG_VERSION;

import static ru.t1.artamonov.tm.constant.ArgumentConst.ARG_HELP;

import static ru.t1.artamonov.tm.constant.ArgumentConst.ARG_ABOUT;

public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name:    Anatoly Artamonov");
        System.out.println("E-mail:  aartamonov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Display program version.\n", CMD_VERSION, ARG_VERSION);
        System.out.printf("%s, %s - Display developer info.\n", CMD_ABOUT, ARG_ABOUT);
        System.out.printf("%s, %s - Display list of terminal commands.\n", CMD_HELP, ARG_HELP);
        System.out.printf("%s - Exit from application.\n", CMD_EXIT);
    }

    private static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }


    private static void showErrorArgument() {
        System.err.println("Error! This argument not supported...");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command not supported...");
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case CMD_HELP:
                showHelp();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_EXIT:
                System.exit(0);
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void processCommands() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("\nENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ARG_HELP:
                showHelp();
                break;
            case ARG_VERSION:
                showVersion();
                break;
            case ARG_ABOUT:
                showAbout();
                break;
            default:
                showErrorArgument();
                break;
        }
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length < 1) {
            return false;
        }
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
