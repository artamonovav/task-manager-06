package ru.t1.artamonov.tm.constant;

public final class ArgumentConst {

    public static final String ARG_HELP = "-h";

    public static final String ARG_VERSION = "-v";

    public static final String ARG_ABOUT = "-a";

}
